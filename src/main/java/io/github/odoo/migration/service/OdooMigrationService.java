package io.github.odoo.migration.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.github.odoo.migration.service.dto.ConfigDiffDTO;
import io.github.odoo.migration.service.dto.InputDTO;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;

@Service
public class OdooMigrationService {
    private String embulkURL = "http://localhost:8080/api";
    private String API_MONGODB_EMBULK_LOAD_OUTPUT_URL = "/mongodb-to-postgresql";
    private String API_MYSQL_EMBULK_LOAD_OUTPUT_URL = "/mysql-to-postgresql";

    private ResponseEntity<?> callApiToGetData(final Object inputDTO, final String url, final Class<?> cls)
        throws JsonProcessingException
    {
        ObjectMapper mapper = new ObjectMapper();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(APPLICATION_JSON_UTF8);
        HttpEntity<String> entity = new HttpEntity<String>(mapper.writeValueAsString(inputDTO), httpHeaders);
        RestTemplate restTemplate = new RestTemplate();

        return restTemplate.exchange(url,
            HttpMethod.POST,
            entity,
            cls);
    }
    public ConfigDiffDTO InputDTOToConfigDTO(InputDTO inputDTO){
        ConfigDiffDTO configDiffDTO = new ConfigDiffDTO();
        configDiffDTO.setDatabaseType(inputDTO.getIn().getType());
        configDiffDTO.setTableName(inputDTO.getOut().getTable());
        configDiffDTO.setConfigDiff(inputDTO.toJsonString());
        return configDiffDTO;
    }
    public String callEmbulk(ConfigDiffDTO configDiffDTO) throws IOException {
        ConfigDiffDTO outputConfigDiff ;
        if (configDiffDTO.getDatabaseType().equals("mongodb")) {
            ResponseEntity<?> result = callApiToGetData(configDiffDTO,
                embulkURL + API_MONGODB_EMBULK_LOAD_OUTPUT_URL,
                ConfigDiffDTO.class);
            outputConfigDiff = (ConfigDiffDTO) result.getBody();
        } else {
            ResponseEntity<?> result = callApiToGetData(configDiffDTO,
                embulkURL + API_MYSQL_EMBULK_LOAD_OUTPUT_URL,
                ConfigDiffDTO.class);
            outputConfigDiff = (ConfigDiffDTO) result.getBody();
        }

        if (outputConfigDiff == null) {
            String log_string = "Can't get Config Diff with data: " + configDiffDTO.getConfigDiff();
            return log_string;
        }
        return "";
    }
}
