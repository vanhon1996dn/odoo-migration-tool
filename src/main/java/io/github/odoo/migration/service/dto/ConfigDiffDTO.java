package io.github.odoo.migration.service.dto;

public class ConfigDiffDTO {
    private String ConfigDiff;
    private String databaseType;
    private String tableName;

    public String getConfigDiff() {
        return ConfigDiff;
    }

    public void setConfigDiff(String configDiff) {
        ConfigDiff = configDiff;
    }

    public String getDatabaseType() {
        return databaseType;
    }

    public void setDatabaseType(String databaseType) {
        this.databaseType = databaseType;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }


}
