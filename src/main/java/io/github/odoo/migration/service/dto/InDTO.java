package io.github.odoo.migration.service.dto;

public class InDTO {
    private String type;
    private String host;
    private String port;
    private String user;
    private String password;
    private String database;
    private String table;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }


    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public String toJsonString() {
        if(this.type.equals("mongodb")){
            return "\"in\":{\"type\":" +type+
                ",\"hosts\":[{\"host\":\"" +host+
                "\",\"port\":\"" +port+
                "\"}],\"user\":\"" +user+
                "\",\"password\":\"" +password+
                "\",\"database\":\"" +database+
                "\",\"collection\":\"" +table+
                "\"}";

        }else {
            return "\"in\":{\"type\":\"" +type+
                "\",\"host\":\"" +host+
                "\",\"port\":\"" +port+
                "\",\"user\":\"" +user+
                "\",\"password\":\"" +password+
                "\",\"database\":\"" +database+
                "\",\"table\":\"" +table+
                "\"}";
        }
    }
}
