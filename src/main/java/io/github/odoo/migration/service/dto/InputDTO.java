package io.github.odoo.migration.service.dto;

import io.github.odoo.migration.service.util.DefaultValue;

import java.util.*;

public class InputDTO {
    private InDTO in;
    private List<ColumnDTO> columns;
    private OutDTO out;
    private static DefaultValue default_value = new DefaultValue();
    public InDTO getIn() {
        return in;
    }

    public void setIn(InDTO in) {
        this.in = in;
    }

    public List<ColumnDTO> getColumns() {
        return columns;
    }

    public void setColumns(List<ColumnDTO> columns) {
        this.columns = columns;
    }

    public OutDTO getOut() {
        return out;
    }

    public void setOut(OutDTO out) {
        this.out = out;
    }
    public String toJsonString(){

        if(this.in.getType().equals("mongodb")){
            return "{" + this.in.toJsonString()+
                ",\"filters\":[" +
                 this.columnsToExpandJsonFilter() +
                 this.columnsToColumnFilter() +
                "]," + this.out.toJsonString() + "}";
        }else{
            return "{" + this.in.toJsonString()+
                ",\"filters\":[" +
                this.columnsToColumnFilter() +
                "]," + this.out.toJsonString() + "}";

        }
    }

    private String columnsToExpandJsonFilter(){
        if(columns.size() > 0){
            String columns_string = "{" +
                "\"type\": \"expand_json\"," +
                "\"json_column_name\": \"record\"," +
                "\"expanded_columns\": [";
            for (ColumnDTO colum:columns){
                columns_string = columns_string.concat("{\"name\":\"" +colum.getOldName() +
                    "\",\"type\":\"" + colum.getNewType() +
                    "\"},");
            }
            columns_string = columns_string.substring(0,columns_string.length() -1);
            columns_string = columns_string + "]},";
            return columns_string;
        }

        return "";
    }

    private String columnsToColumnFilter(){
        if(columns.size() > 0){
            String columns_string = "{" +
                "\"type\": \"column\"," +
                "\"columns\": [";
            for (ColumnDTO colum:columns){
                columns_string = columns_string.concat("{\"name\":\"" +colum.getNewName() +
                    "\",\"src\":\"" + colum.getOldName() +
                    "\",\"type\":\"" + colum.getNewType() +
                    "\",\"default\":\"" + default_value.getDefaultValueOf(colum.getNewType()) +
                    "\"},");
            }
            columns_string = columns_string.substring(0,columns_string.length() -1);
            columns_string = columns_string + "]}";
            return columns_string;
        }
       return "";
    }


}
