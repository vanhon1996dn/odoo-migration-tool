package io.github.odoo.migration.service.dto;

public class OutDTO {
        private String type;
        private String host;
        private Long port;
        private String user;
        private String password;
        private String database;
        private String table;
        private String mode;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getHost() {
            return host;
        }

        public void setHost(String host) {
            this.host = host;
        }

        public Long getPort() {
            return port;
        }

        public void setPort(Long port) {
            this.port = port;
        }

        public String getUser() {
            return user;
        }

        public void setUser(String user) {
            this.user = user;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getDatabase() {
            return database;
        }

        public void setDatabase(String database) {
            this.database = database;
        }

        public String getTable() {
            return table;
        }

        public void setTable(String table) {
            this.table = table;
        }

        public String getMode() {
            return mode;
        }

        public void setMode(String mode) {
            this.mode = mode;
        }

        public String toJsonString() {
                return "\"out\":{\"type\":\"" +type+
                    "\",\"host\":\"" +host+
                    "\",\"port\":\"" +port+
                    "\",\"user\":\"" +user+
                    "\",\"password\":\"" +password+
                    "\",\"database\":\"" +database+
                    "\",\"table\":\"" +table+
                    "\",\"mode\":\"" +mode+
                    "\"}";
        }
    }

