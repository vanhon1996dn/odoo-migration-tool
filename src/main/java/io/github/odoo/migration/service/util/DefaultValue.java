package io.github.odoo.migration.service.util;

import java.util.HashMap;
import java.util.Map;

public class DefaultValue {
    private Map<String,String> default_value = new HashMap<String, String>();

    public DefaultValue() {
        default_value.put("string", "");
        default_value.put("double", "1");
        default_value.put("boolean", "true");
        default_value.put("long", "1");
        default_value.put("json", "{}");
        default_value.put("timestamp", "");
    }
    public String getDefaultValueOf(String key){
        return default_value.get(key);
    }
}
