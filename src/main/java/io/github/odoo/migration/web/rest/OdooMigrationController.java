package io.github.odoo.migration.web.rest;

import io.github.odoo.migration.service.OdooMigrationService;
import io.github.odoo.migration.service.dto.ConfigDiffDTO;
import io.github.odoo.migration.service.dto.InputDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
@RestController
@RequestMapping("/api")
public class OdooMigrationController {
    @Autowired
    private OdooMigrationService odooMigrationService;
    @PostMapping(value = "/odoo_migration", consumes = {"application/json"})
    public ResponseEntity authorize( @RequestBody InputDTO InputDTO) {
        try{
            ConfigDiffDTO configDiffDTO =  odooMigrationService.InputDTOToConfigDTO(InputDTO);
            String result = odooMigrationService.callEmbulk(configDiffDTO);

            if(result.equals("")){
                return new ResponseEntity(HttpStatus.OK);
            }else{
                return new ResponseEntity( HttpStatus.BAD_REQUEST);

            }
        }catch (Exception e){
            String result = "Can't connect Server";
            return new ResponseEntity(HttpStatus.NOT_ACCEPTABLE);
        }

    }


}
