/**
 * View Models used by Spring MVC REST controllers.
 */
package io.github.odoo.migration.web.rest.vm;
