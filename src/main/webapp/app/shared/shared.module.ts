import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {
  OdooMigrationToolSharedLibsModule,
  OdooMigrationToolSharedCommonModule,
  JhiLoginModalComponent,
  HasAnyAuthorityDirective
} from './';

@NgModule({
  imports: [OdooMigrationToolSharedLibsModule, OdooMigrationToolSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [OdooMigrationToolSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class OdooMigrationToolSharedModule {
  static forRoot() {
    return {
      ngModule: OdooMigrationToolSharedModule
    };
  }
}
